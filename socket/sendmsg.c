#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>      /*bzero*/
#include<sys/types.h>
#include<sys/socket.h>

int main(int argc, char *argv[]){
    int ret;
    int socket_pair[2];
    struct msghdr msg;
    struct iovec iov[1];
    char send_buf[100] = "It is a test";

    //接收
    struct msghdr msgr;
    struct iovec iovr[1];
    char recv_buf[100];


    /*创建套接字*/
    ret = socketpair(AF_LOCAL, SOCK_STREAM, 0, socket_pair);
    if(ret == -1){
        fprintf(stderr, "socketpair error\n");
        exit(EXIT_FAILURE);
    }

    /*socket_pair[1]发送数据到本机*/
    bzero(&msg, sizeof(msg));   /*能够将内存块（字符串）的前n个字节清零*/
    msg.msg_name = NULL;        /* void*类型 NULL本地地址*/
    msg.msg_namelen = 0;
    iov[0].iov_base = send_buf;
    iov[0].iov_len = sizeof(send_buf);
    msg.msg_iov = iov;          //要发送或接受数据设为iov
    msg.msg_iovlen = 1;         //1个元素


    printf("开始发送数据：\n");
    printf("发送的数据为: %s\n", send_buf);
    ret = sendmsg(socket_pair[1], &msg, 0);
    if(ret == -1 ){
        fprintf(stderr, "sendmsg err\n");
        exit(EXIT_FAILURE);
    }

    printf("发送成功！\n");

    /* 通过sock[0]接收发送过来的数据 */
    bzero(&msg, sizeof(msg));
    msgr.msg_name = NULL;
    msgr.msg_namelen = 0;
    iovr[0].iov_base = &recv_buf;
    iovr[0].iov_len = sizeof(recv_buf);
    msgr.msg_iov = iovr;
    msgr.msg_iovlen = 1;

    ret = recvmsg(socket_pair[0], &msgr, 0);
    if(ret == -1){
        fprintf(stderr, "recvmsg error\n");
        exit(EXIT_FAILURE);
    }
    printf("接收成功!\n");
    printf("收到数据为: %s\n", recv_buf);

    /* 关闭sockets */
    close(socket_pair[0]);
    close(socket_pair[1]);

    return 0;
}