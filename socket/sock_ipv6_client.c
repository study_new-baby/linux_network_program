#include<stdio.h>  
#include<stdlib.h>  
#include<string.h>  
#include<errno.h>  
#include<unistd.h>
#include<sys/types.h>  
#include<sys/socket.h>  
#include<arpa/inet.h>

#define MAX_LEN 4096  
  
int main(int argc, char* argv[])  
{  
    int clifd;                                      //客户端socket
    struct sockaddr_in6 seraddr;  
    char recvbuf[MAX_LEN], sendbuf[MAX_LEN];        //分别保存发送和接收内容
    int ret;                                        //记录recv的字节数
  
    if( argc != 2){  
        printf("usage: ./client <ip6address>\n");    //要ping 服务端的ip地址
        exit(EXIT_FAILURE);  
    }  
  
    if( (clifd = socket(AF_INET6, SOCK_STREAM, 0)) == -1 ){  
        printf("create socket error: %s(errno: %d)\n", strerror(errno),errno);  
        exit(EXIT_FAILURE);  
    }  
  
    memset(&seraddr, 0, sizeof(seraddr));  

    seraddr.sin6_family = AF_INET6;  
    seraddr.sin6_port = htons(6666);                             //端口6666

    if( inet_pton(AF_INET, "fe80::a00:27ff:fe06:5688", &seraddr.sin6_addr) == -1 ){   //将输入的ip地址转换成电脑可识别的二进制数值格式,保存到seraddr中
        printf("inet_pton error for %s\n",argv[1]);  
        exit(EXIT_FAILURE);  
    }  
  
    //连接服务端ip地址
    if( (connect(clifd, (struct sockaddr*)&seraddr, sizeof(seraddr))) == -1 ){
        printf("connect error: %s(errno: %d)\n",strerror(errno),errno);  
        exit(EXIT_FAILURE);  
    }  
  
    //开始发送信息
    printf("\033[1;33m|=======send msg to server======|\033[5m\n");  
    fgets(sendbuf, MAX_LEN, stdin);                         //获取输入内容
    if( send(clifd, sendbuf, sizeof(sendbuf), 0) == -1 ){  
        printf("send msg error: %s(errno: %d)\n", strerror(errno), errno);  
        exit(EXIT_FAILURE);  
    }  
  
    close(clifd);  
   
    return 0;
}  