#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<stdlib.h>
#include<unistd.h>


const char* str = "SOCKET PAIR TEST";

int main(int argc, char *argv[]){
    int socket_pair[2];
    char buf[128] = {0};
    pid_t pid;

    if(socketpair(AF_UNIX, SOCK_STREAM, 0, socket_pair) == -1){
        fprintf(stderr, "socketpair error\n");
        exit(EXIT_FAILURE);
    }

    //不同进程就是指在父子进程中读/写，前提需要关闭另一端
    pid = fork();
    switch(pid){
        case -1:{
            fprintf(stderr, "fork error\n");
            exit(EXIT_FAILURE);
        };
        case 0:{ //child
            close(socket_pair[1]);   //关闭1端
            int size = write(socket_pair[0], str, strlen(str) + 1); //0端写入
            printf("Write success, pid: %d\n", getpid());
        };
        default:{  //parent
            close(socket_pair[0]);   //关闭0端
            //从管道0端写入，从1端读出, 保存到buf
            read(socket_pair[1], buf, sizeof(buf));   
            printf("Read result : %s\n", buf);
        }
    }
    // for(;;) {
    //     sleep(1);           //延迟，不让进程之间退出
    // }

    return 0;   
}