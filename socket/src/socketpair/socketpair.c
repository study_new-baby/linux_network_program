#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<stdlib.h>
#include<unistd.h>


const char* str = "SOCKET PAIR TEST";

int main(int argc, char *argv[]){
    int socket_pair[2];
    char buf[128] = {0};
    pid_t pid;

    if(socketpair(AF_UNIX, SOCK_STREAM, 0, socket_pair) == -1){
        fprintf(stderr, "socketpair error\n");
        exit(EXIT_FAILURE);
    }

    int size = write(socket_pair[0], str, strlen(str) + 1);
    //从管道0端写入，从1端读出, 保存到buf
    read(socket_pair[1], buf, size);            
    printf("Read result : %s\n", buf);
    return 0;   
}