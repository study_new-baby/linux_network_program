#include<stdio.h>  
#include<stdlib.h>  
#include<string.h>  
#include<unistd.h>
#include<sys/socket.h>  
#include<arpa/inet.h>
#include<netinet/in.h>

#define MAX_LEN 4096  

int main(int argc, char* argv[])  
{  
    int clifd;
    struct sockaddr_in seraddr;
    struct sockaddr srcaddr;
    socklen_t len;
    char recvbuf[MAX_LEN], sendbuf[MAX_LEN];        //分别保存发送和接收内容
    int ret, read_len;

    if( argc != 2){  
        printf("usage: ./client <ipaddress>\n");    //要ping 服务端的ip地址
        exit(EXIT_FAILURE);  
    } 

    clifd = socket(AF_INET, SOCK_DGRAM, 0);
    if(clifd == -1){
        fprintf(stderr, "socket error\n");
        exit(EXIT_FAILURE);
    }

    bzero(&seraddr, sizeof(seraddr));
    seraddr.sin_family = AF_INET;
    seraddr.sin_port = htons(6666);
    if( inet_pton(AF_INET, argv[1], &seraddr.sin_addr) == -1 ){   //将输入的ip地址转换成电脑可识别的二进制数值格式,保存到seraddr中
        printf("inet_pton error for %s\n",argv[1]);  
        exit(EXIT_FAILURE);  
    }  

    while(1){
        //开始发送信息
        printf("\033[1;33m|=======send msg to server======|\033[5m\n");  
        fgets(sendbuf, MAX_LEN, stdin);                         //获取输入内容
        if( sendto(clifd, sendbuf, sizeof(sendbuf), 0, (struct sockaddr*)&seraddr, sizeof(seraddr)) == -1 ){  
            printf("send msg error\n");  
            exit(EXIT_FAILURE);  
        }  

        memset(recvbuf, 0, sizeof(recvbuf));
        read_len = recvfrom(clifd, recvbuf, sizeof(recvbuf), 0, &srcaddr, &len);
        printf("recv msg from server is: %s\n", recvbuf);
    }
      
    close(clifd);  
   
    return 0;
}