#include<sys/types.h>
#include<sys/socket.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/time.h>
#include<string.h>
#include<net/if.h>
#include<sys/ioctl.h>
#include<netinet/in.h>/*struct sockaddr_in定义的头文件*/
#include<arpa/inet.h>

#define IFI_NAME	16
#define	IFI_HADDR	8	

typedef struct ifi_info	ifi_info_st;
struct	ifi_info{
	char	name[IFI_NAME];
	short	index;
	short	mtu;
	u_char	haddr[IFI_HADDR];
	u_short	hlen;
	short	flags;
	short 	myflags;
	int 	Metric;
	struct	sockaddr	*addr;
	struct	sockaddr	*brdaddr;
	struct 	sockaddr	*dstaddr;
	struct 	sockaddr	*netmask;
	struct 	ifmap		*if_map;
	ifi_info_st	*next;
};
/*释放开辟的空间*/
void free_mem(void*ifihead)
{
	ifi_info_st *ifi = (ifi_info_st*)ifihead,*ifinext;
	while(ifi){
		ifinext = ifi->next;
		if(ifi->addr != NULL)
			free(ifi->addr);
		if(ifi->brdaddr != NULL)
			free(ifi->brdaddr);
		if(ifi->dstaddr != NULL)
			free(ifi->dstaddr);
		if(ifi->netmask != NULL)
			free(ifi->dstaddr);	
		if(ifi->if_map != NULL)
			free(ifi->if_map);				
		free(ifi);
		ifi = ifinext;
	}
}
/*获取所有接口的信息*/
ifi_info_st*  get_ifi_info(int family,int doaliases)
{
	ifi_info_st *ifi,*ifihead,**ifinext;
	int sockfd,len,lastlen,flags,myflags,idx = 0,hlen = 0,ret ;
	char *ptr,*buf,lastname[IFI_NAME],*cptr,*haddr,*sdlname;
	struct ifconf ifc;
	struct ifreq	*ifr,ifrcopy;
	struct sockaddr	sinptr;
	/*socket的类型可以是TCP也可以是UDP或其它的*/
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	/*试探获取buf的长度*/
	lastlen = 0;
	len = 100*sizeof(struct ifreq);
	for(;;){
		buf = malloc(len);
		if(buf == NULL){perror("malloc failed");close(sockfd);exit(-1);}
		ifc.ifc_len = len;
		ifc.ifc_buf = buf;
		if(ioctl(sockfd,SIOCGIFCONF,&ifc)<0){
			perror("ioctl failed\n");
		}else{
			if(ifc.ifc_len == lastlen){
				break;
			}
			lastlen = ifc.ifc_len;
		}
		len += 10*sizeof(struct ifreq);
		free(buf);
	}
	
	ifihead = NULL;
	ifinext = &ifihead;
	lastname[0] = 0;
	/*解析各个网络接口、以及获取网络接口中的其它信息*/
	for(ptr = buf;ptr < buf + ifc.ifc_len;){
		ifr = (struct ifreq*)ptr;
		len = sizeof(struct sockaddr);
		ptr += sizeof(struct ifreq);
		
		if(ifr->ifr_addr.sa_family != family)
			continue;
		myflags = 0;
		if((cptr = strchr(ifr->ifr_name,':')) != NULL)
			*cptr = 0;
		if(strncmp(lastname,ifr->ifr_name,IFI_NAME) == 0){
			myflags = 1;
			if(doaliases == 0){continue;}
		}
		memcpy(lastname,ifr->ifr_name,IFI_NAME);
		/*获取flag，参见的UP、BOARDCAT等*/
		ifrcopy = *ifr;
		ioctl(sockfd,SIOCGIFFLAGS,&ifrcopy);
		flags = ifrcopy.ifr_flags;
		
		if((flags & IFF_UP) == 0)
			continue;
		ifi = calloc(1,sizeof(ifi_info_st));
		//memset(ifi,0,sizeof(ifi_info_st));
		*ifinext = ifi;
		ifinext = &ifi->next;
		ifi->flags = flags;
		ifi->myflags = myflags;
		/*获取MTU*/
		ioctl(sockfd,SIOCGIFMTU,&ifrcopy);
		ifi->mtu = ifrcopy.ifr_mtu;
		memcpy(ifi->name,ifr->ifr_name,sizeof(ifr->ifr_name));
		ifi->name[IFI_NAME-1] = 0;
		
		sinptr = ifr->ifr_addr;
		ifi->addr = calloc(1,sizeof(struct sockaddr));
		memcpy(ifi->addr,&sinptr,sizeof(struct sockaddr));
		/*获取广播地址*/
		if(flags & IFF_BROADCAST){
			ioctl(sockfd,SIOCGIFBRDADDR,&ifrcopy);
			sinptr = ifrcopy.ifr_broadaddr;
			ifi->brdaddr = calloc(1,sizeof(struct sockaddr));
			memcpy(ifi->brdaddr,&sinptr,sizeof(struct sockaddr));
		}
		/*获取metric的值，但是这里获取到的是0？？请求指点*/
		ret = ioctl(sockfd,SIOCGIFMETRIC,&ifrcopy);
		if(ret == -1){perror("ioctl SIOCGIFMETRIC failed");}
		ifi->Metric = ifrcopy.ifr_metric;
		/*获取硬件地址*/
		ret = ioctl(sockfd,SIOCGIFHWADDR,&ifrcopy);
		if(ret == -1){perror("ioctl SIOCGIFHWADDR failed");}
		memcpy(ifi->haddr,ifrcopy.ifr_hwaddr.sa_data,6/*sizeof(ifrcopy.ifr_hwaddr.sa_data)*/);
		ifi->hlen = 6;
		/*获取子网掩码*/
		ret = ioctl(sockfd,SIOCGIFNETMASK,&ifrcopy);
		if(ret == -1){perror("ioctl SIOCGIFNETMASK failed");}
		else{
			ifi->netmask = calloc(1,sizeof(struct sockaddr));
			memcpy(ifi->netmask,&ifrcopy.ifr_netmask,sizeof(struct sockaddr));		
		}
		/*获取网卡的映射参数，有开始地址、结束地址、基地址、中断、DMA、端口等
		*这些参数在网卡驱动中有维持，在struct net_device中保存着
		*/
		ret = ioctl(sockfd,SIOCGIFMAP,&ifrcopy);
		if(ret == -1){perror("get interface map failed");}
		else{
			ifi->if_map = calloc(1,sizeof(struct ifmap));
			if(!ifi->if_map){perror("calloc failed");}else{
				memcpy(ifi->if_map,&ifrcopy.ifr_map,sizeof(struct ifmap));
			}
		}
		
		#if 0
		if(flags & IFF_POINTOPOINT){
			ioctl(sockfd,SIOCGDSTADDR,&ifrcopy);
			sinptr = ifrcopy.ifr_broadaddr;
			ifi->brdaddr = calloc(1,sizeof(struct sockaddr));
			memcpy(ifi->brdaddr,&sinptr,sizeof(struct sockaddr));
		}
		#endif
	}
	free(buf);
	return ifihead;
	
}
/*把获取到的数据转换为点分十进制的字符串类型*/
char* convert_ip(void*s)
{	
	static char buf[32];
	memset(buf,0,sizeof(buf));
	struct sockaddr_in *p = (struct sockaddr_in*)s;
	inet_ntop(AF_INET,&p->sin_addr,buf,sizeof(buf));
	return buf;
	
}
/*获取网卡信息以及打印*/
int main(int argc ,char* argv[])
{
	ifi_info_st	*ifi,*ifihead;
	struct	sockaddr	*sa;
	u_char 	*ptr;
	int i,family,doaliases;
	
	if(argc != 3){printf("usage:prifinfo<inet4 | inet6> <doaliases>\n");exit(-1);}
	if(strcmp(argv[1],"inet4") == 0){
		family = AF_INET;
	}else if(strcmp(argv[1],"inet6") == 0){
		family = AF_INET6;
	}else{
		printf("invalid <address-family>\n");
		exit(-1);
	}
	doaliases = atoi(argv[2]);
	
	for(ifihead = ifi = get_ifi_info(family,doaliases);ifi != NULL;ifi = ifi->next){
		printf("%s: ",ifi->name);
		if(ifi->index != 0){printf("(%d) ",ifi->index);}	
		//printf("<");
		if(ifi->flags & IFF_UP)					printf("UP ");
		if(ifi->flags & IFF_BROADCAST)			printf("BROADCAST ");
		if(ifi->flags & IFF_DEBUG)				printf("DEBUG ");
		if(ifi->flags & IFF_LOOPBACK)			printf("LOOPBACK ");
		if(ifi->flags & IFF_POINTOPOINT)		printf("POINTOPOINT ");
		if(ifi->flags & IFF_RUNNING)			printf("RUNNING ");
		if(ifi->flags & IFF_MULTICAST)			printf("MULTICAST ");
		if(ifi->flags & IFF_PORTSEL)			printf("PORTSEL ");
		if(ifi->flags & IFF_AUTOMEDIA)			printf("AUTOMEDIA ");
		if(ifi->flags & IFF_DYNAMIC)			printf("DYNAMIC ");
		printf("MTU:%d  Metric:%d\n",ifi->mtu,ifi->Metric);
		//printf(">\n");
	
		
		if((i = ifi->hlen) > 0){
			ptr = ifi->haddr;
			printf("HWaddr ");
			do{
				printf("%s%02x",(i == ifi->hlen)?" ": ":",*ptr++);
			}while(--i > 0);printf("\n");
		}
		
		if((sa = ifi->addr) != NULL){
			printf("addr:%s  ",convert_ip(sa));
		}
		if((sa = ifi->brdaddr) != NULL){
			printf("Bcast:%s  ",convert_ip(sa));
		}
		if((sa = ifi->dstaddr) != NULL){
			printf("destination addr:%s\n",convert_ip(sa));
		}
		if((sa = ifi->netmask) != NULL){
			printf(" Mask:%s\n",convert_ip(sa));
		}
	}

	free_mem(ifihead);
	return 0;
}
