#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<ifaddrs.h>
#include<arpa/inet.h>

#include<sys/socket.h>
#include <netdb.h>              /* for getnameinfo NI_MAXHOST */

#include <unistd.h>
#include <net/if.h>             /* for AF_BROCDCAST flags*/


// struct ifaddrs
// {
//   struct ifaddrs *ifa_next;	/* Pointer to the next structure.  */

//   char *ifa_name;		/* Name of this network interface.  */
//   unsigned int ifa_flags;	/* Flags as from SIOCGIFFLAGS ioctl.  */

//   struct sockaddr *ifa_addr;	/* Network address of this interface.  */
//   struct sockaddr *ifa_netmask; /* Netmask of this interface.  */
//   union
//   {
//     struct sockaddr *ifu_broadaddr; 
//     struct sockaddr *ifu_dstaddr; 
//   } ifa_ifu;

int main(int argc, char* argv[]){
    struct ifaddrs *ifaddr, *ifhead;
    int family, s;
    char host[NI_MAXHOST];

    if( getifaddrs(&ifaddr) == -1 ){                //获取网络接口信息
        fprintf(stderr, "getifaddrs error\n");
        exit(EXIT_FAILURE);
    }

    /* 遍历链表，维护头指针，以便稍后可以释放链表  */
    for( ifhead = ifaddr; ifhead != NULL; ifhead = ifhead->ifa_next ){
        if( ifhead->ifa_addr == NULL ){
            continue;
        }
        family = ifhead->ifa_addr->sa_family;       //记录族

        /* 显示接口名称和族 */
        printf( "%s  address family: %d%s\n", \
        ifhead->ifa_name,family,
        ( family == AF_PACKET )? " (AF_PACKET)":
        ( family == AF_INET )? " (AF_INET)":
        ( family == AF_INET6 )? " (AF_INET6)" : " ");

        /* For an AF_INET* interface address, display the address */
        if(family == AF_INET || family == AF_INET6){
            s = getnameinfo(ifhead->ifa_addr, 
            (family == AF_INET) ? sizeof(struct sockaddr_in) : sizeof(struct sockaddr_in6),
            host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);

            if(s == -1){
                fprintf(stderr, "getnameinfo error\n");
                exit(EXIT_FAILURE);
            }
        }
        printf("\taddress: <%s>\n", host);

        if(ifhead->ifa_flags & IFF_BROADCAST){
            s = getnameinfo(ifhead->ifa_broadaddr,
						(family == AF_INET) ? sizeof(struct sockaddr_in) :
						sizeof(struct sockaddr_in6),
						host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
			printf("\tbroadcast address: <%s>\n", host);
        }

        if (ifhead->ifa_flags & IFF_POINTOPOINT) {
			s = getnameinfo(ifhead->ifa_dstaddr,
                    (family == AF_INET) ? sizeof(struct sockaddr_in) :
                    sizeof(struct sockaddr_in6),
                    host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
			printf("\tbroadcast address: <%s>\n", host);
		}
    }

    freeifaddrs(ifaddr);
    return 0;

}