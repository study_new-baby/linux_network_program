#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdint.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/ioctl.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<net/if.h>      /*for ifconf*/

#define MAC_LEN 6
#define MAC_NUM  10
#define IF_NAME_SIZE 1024
#define DEFAULT_INTERFACE "lo"

struct port_t {
    struct port_t* next;
    int sock;   
    int if_count;                        //记录网卡数量
    uint8_t ifindex;                     //保存网络接口索引值
    char ifname[IF_NAME_SIZE];           //保存网络接口名
    unsigned char Macaddr[MAC_LEN];      //保存物理mac地址 
};

int get_hw_addr(struct port_t *port);
void print_Mac_info(int sock, struct ifreq ifr[], struct port_t *port);
int get_if_name_and_index(struct port_t *port);
// int get_if_name_to_index(struct port_t *port);

int main(int argc, char* argv[]){
    struct port_t port;

    if( get_hw_addr(&port) == -1 ){             //保存mac地址到port->Macaddr
        printf("get HW addr failed\n");
        exit(EXIT_FAILURE);
    }

    if( get_if_name_and_index(&port) == -1 ){         //保存ifindex和ifname到port->ifindex,port->ifname
        printf("get if name and index failed\n");
        exit(EXIT_FAILURE);
    }

    // if( get_if_name_to_index(&port) == -1 ){         //保存ifindex到port->ifindex
    //     printf("Fail to name to index.\n");
    //     exit(EXIT_FAILURE);
    // }
    return 0;
}

int get_hw_addr(struct port_t *port){
    struct ifreq ifr[MAC_NUM];           //用来配置和获取ip地址，掩码，MTU等接口信息的
    struct ifconf ifc;                   //记录Buffer address./* Size of buffer.  */
    int ret = -1;                        //记录ioctl是否获取网卡地址

    memset(&ifc, 0, sizeof(ifc));
    memset(&ifr, 0, sizeof(ifr));

    port->sock = socket(AF_INET, SOCK_STREAM, 0);
    if(port->sock == -1){
        fprintf(stderr, "scoket error\n");
        return -1;
    }

    //分配足够的长度放入所有的网络接口信息
    ifc.ifc_len = MAC_NUM * sizeof(struct ifreq);
    ifc.ifc_buf = (char *)ifr;


    //获取网卡地址信息
    ret = ioctl(port->sock, SIOCGIFCONF, (char *)&ifc);              //获取网络接口列表
    if(ret == -1){
        fprintf(stderr, "ioctl error\n");
        return -1;
    }

    port->if_count = ifc.ifc_len / sizeof(struct ifreq);

    print_Mac_info(port->sock, ifr, port);                 //打印网络接口名与MAC地址

    return 0;
}

void print_Mac_info(int sock, struct ifreq ifr[], struct port_t *port){
    for(int i = 0; i < port->if_count; i++){
        if( ioctl(sock, SIOCGIFHWADDR, &ifr[i]) == 0 ){
            memcpy(port->Macaddr, ifr[i].ifr_hwaddr.sa_data, 6);
            printf("eth: %s, mac: %02x:%02x:%02x:%02x:%02x:%02x\n", ifr[i].ifr_name, \
            port->Macaddr[0], port->Macaddr[1], port->Macaddr[2], port->Macaddr[3], port->Macaddr[4], port->Macaddr[5]);
        }
    }
}

struct port_t* copyif(struct if_nameindex *ifni){
    struct port_t* new = (struct port_t*)malloc(sizeof(struct port_t));
    if(new == NULL) {
        fprintf(stderr, "malloc");
    }
    memset(new, 0, sizeof(*new));

    memcpy(new->ifname, ifni->if_name, sizeof(ifni->if_name));
    new->ifindex = ifni->if_index;

    return new;
}

int get_if_name_and_index(struct port_t *port){
    struct if_nameindex *head, *ifni;
    ifni = if_nameindex();
    head = ifni;
    struct port_t* phead = NULL;
    struct port_t* cur = NULL;
    
    if (head == NULL) {
        perror("if_nameindex()");
        exit(EXIT_FAILURE);
    }   
    
    while (ifni->if_index != 0) {
        struct port_t* new = copyif(ifni);
        new->next = NULL;

        if(phead == NULL){
            phead = new;
            cur = new;
        } else {
            cur->next = new;
            cur = new;
        }
        ifni++;
    }   
    
    cur = phead;
    while(cur != NULL){
        printf("Interfece %d : %s\n", cur->ifindex, cur->ifname);
        cur = cur->next;
    }

    cur = phead;
    while(cur != NULL){
        struct port_t* tmp = cur;
        cur = cur->next;
        free(tmp);
    }

    if_freenameindex(head);
    head = NULL;
    ifni = NULL;
    
    return 0;
}

// int get_if_name_to_index(struct port_t *port){
//     port->ifindex = if_nametoindex(port->ifname);
//     if (port->ifindex == 0) {
//         fprintf(stderr, "Interface %s : No such device\n", port->ifname);
//         exit(EXIT_FAILURE);
//     }

//     return 0;
// }