#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <net/if.h>
 
int main(int argc, char *argv[])
{
    if (argc != 2) {
        fprintf(stderr, "Usage: %s [interface index]\n", argv[0]);
        exit(EXIT_FAILURE);
    }   
 
    int saved_errno = errno;
    char if_name[IFNAMSIZ] = {'\0'};
    unsigned int if_index = (unsigned int )atoi(argv[1]);
 
    char *name = if_indextoname(if_index, if_name);
    if (name == NULL && errno == ENXIO) {
        fprintf(stderr, "Index %d : No such device\n", if_index);
        exit(EXIT_FAILURE); 
    } 
    
    errno = saved_errno;  
 
    printf("Index %d : %s\n", if_index, if_name);
 
    exit(EXIT_SUCCESS);
}