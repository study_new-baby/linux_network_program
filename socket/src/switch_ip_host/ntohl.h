#include<stdio.h>

typedef unsigned short uint16;
typedef unsigned long uint32;

/*短整型高低字节交换
16位
15141312111098 76543210
将高位15141312111098移动8位到原先76543210的位置上
*/
#define Swap16(A) (( ((uint16)(A) & 0xff00) >> 8 ) |  ( ((uint16)(A) & 0x00ff) << 8 ) )

/*长整型高低字节交换
32位
*/
#define Swap32(A) ( (((uint32)(A) & 0xff000000) >> 24 ) | \
                    (((uint32)(A) & 0x00ff0000) >> 8 )  | \
                    (((uint32)(A) & 0x0000ff00) << 8 )  | \
                    (((uint32)(A) & 0x000000ff) << 24 ) )

static union{
    char c[4];
    unsigned long mylong;
}endian_test = {{'l','?','?','b'}};

#define ENDIANNESS ((char) endian_test.mylong)