#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <unistd.h>

#include "rpc.h"

void handle_rpc(int client_socket)
{
    char buffer[BUFFER_SIZE];
    int bytes_received = recv(client_socket, buffer, sizeof(rpc_request_t), 0);
    if(bytes_received > 0)
    {
	rpc_request_t *request = (rpc_request_t*)buffer;
	rpc_response_t response;

	switch(request->op)
	{
	    case ADD:
		 response.result = request->op1 + request->op2;
		 break;
	    case SUBTRACT:
		 response.result = request->op1 - request->op2;
		 break;
	    default:
		 fprintf(stderr,"Unknown operation!\n");
		 close(client_socket);
		 return;
	}
	send(client_socket, &response, sizeof(response), 0);
    }
    close(client_socket);
}

int main(int argc, char *argv[])
{
    int ret = -1;
    int server_socket, client_socket;
    struct sockaddr_un server_addr, client_addr;
    socklen_t client_addr_len = sizeof(client_addr);

    //1. create socket
    server_socket = socket(AF_LOCAL,SOCK_STREAM, 0);
    if(server_socket == -1)
    {
	perror("socket");
	exit(EXIT_FAILURE);
    }

    //2. config sun_family and sun_path
    memset(&server_addr,0,sizeof(server_addr));
    server_addr.sun_family = AF_LOCAL;
    strncpy(server_addr.sun_path, RPC_PATH, sizeof(server_addr.sun_path) - 1);
    
    // RPC_PATH exist
    if(access(RPC_PATH,F_OK) == 0)
    {
        unlink(RPC_PATH);
    }

    //3. bind socket, create RPC_PATH
    ret = bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if(ret == -1)
    {
	perror("bind");
	close(server_socket);
	exit(EXIT_FAILURE);
    }

    //4. listen connect
    ret = listen(server_socket, 5);
    if(ret == -1)
    {
	perror("listen");
	close(server_socket);
	exit(EXIT_FAILURE);
    }

    printf("RPC server is waiting for a connection........\n");
    //5. while (1)
    while(1)
    {
        //6. accept connection
	client_socket = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len);
	if(client_socket == -1)
	{
            perror("accept");
	    continue;
	}
	//7. handle rpc request
	handle_rpc(client_socket);
    }
    //8. close socket
    close(server_socket);
    unlink(RPC_PATH);
}
