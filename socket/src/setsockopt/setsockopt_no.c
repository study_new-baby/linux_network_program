#include<stdio.h>
#include<stdlib.h>  
#include<string.h>                  /* for bzero */
#include<unistd.h>                  /* for close */
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>              /* for sockaddr_in */


int main(int argc, char* argv[]){
    int sockfd1;
    int bind_ret;

    sockfd1 = socket(AF_INET,SOCK_DGRAM, 0);
    if(sockfd1 == -1){
        fprintf(stderr, "socket error\n");
        exit(EXIT_FAILURE);
    }

    /* 设置本地网络信息 */
    struct sockaddr_in myaddr;       
    bzero(&myaddr, sizeof(myaddr));

    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(6666);
    myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* 绑定，端口为6666 */
    bind_ret = bind(sockfd1, (struct sockaddr*)&myaddr, sizeof(myaddr));
    if(bind_ret == -1){
        fprintf(stderr, "bind error\n");
        exit(EXIT_FAILURE);
    }

    int sockfd2;
    sockfd2 = socket(AF_INET,SOCK_DGRAM, 0);
    if(sockfd2 == -1){
        fprintf(stderr, "socket error\n");
        exit(EXIT_FAILURE);
    }

    // 因为8000端口已被占用，默认情况下，端口没有释放，无法绑定
	bind_ret = bind(sockfd2, (struct sockaddr*)&myaddr, sizeof(myaddr));
	if(bind_ret == -1)
	{
		perror("bind sockfd_two");
		close(sockfd2);		
		exit(EXIT_FAILURE);
	}

    close(sockfd1);
    close(sockfd2);

    return 0;
}