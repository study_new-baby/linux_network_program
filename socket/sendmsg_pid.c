#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>      /*bzero*/
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/wait.h>

int main(int argc, char* argv[]){
    struct msghdr msg;          /* send */
    struct iovec iov[1];
    int sock_pipe[2];
    char send_buf[100];
    pid_t pid;
    int ret;

    struct msghdr msgr;         /* recv */
    struct iovec iovr[1];
    char recv_buf[100];


    ret = socketpair(AF_LOCAL, SOCK_STREAM, 0, sock_pipe);
    if(ret == -1){
        fprintf(stderr, "socketpair error\n");
        exit(EXIT_FAILURE);
    }

    /*sock_pair[1]发送数据到本机*/
    bzero(&msg, sizeof(msg));   /*能够将内存块（字符串）的前n个字节清零*/
    msg.msg_name = NULL;        /* void*类型 NULL本地地址*/
    msg.msg_namelen = 0;
    iov[0].iov_base = send_buf;
    iov[0].iov_len = sizeof(send_buf);
    msg.msg_iov = iov;
    msg.msg_iovlen = 1;


    /* 通过sock_pair[0]接收发送过来的数据 */
    bzero(&msgr, sizeof(msgr));
    msgr.msg_name = NULL;
    msgr.msg_namelen = 0;
    iovr[0].iov_base = &recv_buf;
    iovr[0].iov_len = sizeof(recv_buf);
    msgr.msg_iov = iovr;
    msgr.msg_iovlen = 1;


    pid = fork();
    switch (pid)
    {
        case -1:{
            fprintf(stderr, "fork error\n");
        }
            break;
        case 0:{     /* child */
            close(sock_pipe[0]);
            printf("开始发送数据：\n");
            fgets(send_buf, 100, stdin);

            ret = sendmsg(sock_pipe[1], &msg, 0);
            if(ret == -1 ){
                fprintf(stderr, "sendmsg err\n");
                exit(EXIT_FAILURE);
            }
            printf("发送成功！\n");

        }
        default:{           /* parent */
            close(sock_pipe[1]);
            ret = recvmsg(sock_pipe[0], &msgr, 0);
            if(ret == -1){
                fprintf(stderr, "recvmsg error\n");
                exit(EXIT_FAILURE);
            }
            printf("接收成功!\n");
            printf("收到数据为: %s\n", recv_buf);

            wait(NULL);
        }
        break;
    }
    
    /* 关闭sockets */
    close(sock_pipe[0]);
    close(sock_pipe[1]);

    return 0;
}