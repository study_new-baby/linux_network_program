#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "../../include/sock_opts.h"

static charstrres[128];
static char *
sock_str_flag(union val *ptr, int len){
    if(len != sizeof(int)){
        snprintf(charstrres, sizeof(charstrres), "size (%d) not sizeof(int)",len);
    } else {
        snprintf(charstrres, sizeof(charstrres), "%s", (ptr->i_val == 0) ? "off":"on");
    }
    return charstrres;
}