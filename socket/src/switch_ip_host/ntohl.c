#include "ntohl.h"

/*
ENDIANNESS返回结果
	l:小端模式
	b:大端模式
*/

/*将主机的无符号短整型数转换成网络字节顺序 
* 小端 -> 大端*/
uint16 htons(uint16 hs){
    return (ENDIANNESS == 'l') ? Swap16(hs) : hs;
}

/*将主机的无符号长整型数转换成网络字节顺序
* 小端 -> 大端*/
uint32 htonl(uint32 hl){
    return (ENDIANNESS == 'l') ? Swap32(hl) : hl;
}

/*将一个无符号短整形数从网络字节顺序转换为主机字节顺序
* 大端 -> 小端*/
uint16 ntohs(uint16 ns){
    return (ENDIANNESS == 'l') ? Swap16(ns) : ns;
}


/*将一个无符号长整形数从网络字节顺序转换为主机字节顺序
* 大端 -> 小端*/
uint32 ntohl(uint32 nl){
    return (ENDIANNESS == 'l') ? Swap32(nl) : nl;
}


int main(int argc, char* argv[]){
    uint16 hs = 0x1234;
    uint16 ns = 0x3412;
    uint32 hl = 0x12345678;
    uint32 nl = 0x78563412;

    printf(" htons(0x%4x) = 0x%4x\n",hs, htons(hs));
    printf(" ntons(0x%4x) = 0x%4x\n",ns, ntohs(ns));
    printf(" htonl(0x%8lx) = 0x%8lx\n",hl, htonl(hl));
    printf(" ntohl(0x%8lx) = 0x%8lx\n",nl, ntohl(nl));
    return 0;
}