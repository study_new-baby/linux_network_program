#include<stdio.h>
#include<stdlib.h>
#include<arpa/inet.h>

#define INET_ADDRSTRLEN 16

int main(int argc, char* argv[]){
    struct in_addr addr1, addr2;
    char _ip[INET_ADDRSTRLEN]  = {0};
    char _ipv4[INET_ADDRSTRLEN]  = {0};
    int l1, l2;

    l1 = inet_pton(AF_INET, "192.168.0.74", _ip);  
    l2 = inet_pton(AF_INET, "211.100.21.179", _ip);  
    printf("%d %d\n", l1, l2);   
    printf("%s\n", _ip); 
    //返回值：若成功则为1，若输入不是有效的表达式则为0，若出错则为-1

    inet_ntop(AF_INET, _ip, _ipv4, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, _ip, _ipv4, INET_ADDRSTRLEN);
    
    printf("%s\n", _ip);                //二进制格式        
    printf("%s\n", _ipv4);              //会被覆盖，呈现最后一个ip地址

    return 0;
}