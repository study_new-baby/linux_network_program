#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<sys/un.h>

int main(int argc, char *argv[]){
    int clifd;
    int ret;
    char buf[100] = {0};

    clifd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if(clifd == -1){
        fprintf(stderr, "socket error\n");
        exit(EXIT_FAILURE);
    }

    /* 创建本身客户端ip地址 */
    struct sockaddr_un client_addr;
    memset(&client_addr, 0, sizeof(client_addr));
    client_addr.sun_family = AF_UNIX;
    strcpy(client_addr.sun_path, "./my_local_cli");  //套节字文件
    socklen_t clen = sizeof(client_addr);


    ret = bind(clifd, (struct sockaddr*)&client_addr, clen);
    if(ret == -1){
        fprintf(stderr, "bind error\n");
        exit(EXIT_FAILURE);
    }

    /* 下面查找服务端的地址 */
    struct sockaddr_un server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, "./my_local");  //套节字文件
    socklen_t slen = sizeof(server_addr);


    while(1){
        printf("请输入 : ");
        fgets(buf,sizeof(buf), stdin);
        buf[strlen(buf) - 1] = '\0'; //清除 \n

        //发送数据
        if( -1 == (sendto(clifd, buf, sizeof(buf), 0, (struct sockaddr*)&server_addr, slen))){
            fprintf(stderr, "sendto error\n");
            exit(EXIT_FAILURE);
        }

        //接收应答
        if(-1 == (recvfrom(clifd, buf, sizeof(buf), 0, (struct sockaddr*)&server_addr, &slen))){
            fprintf(stderr, "recvfrom error\n");
            exit(EXIT_FAILURE);
        }
        printf("服务器应答[%s]\n", buf);
    }

    //关闭监听套接字  一般不关闭
    close(clifd);

    printf("客户端发送成功! ");

    return 0;
}