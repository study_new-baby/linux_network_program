#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <arpa/inet.h>

#include "ndp.h"

#define BUFFER_SIZE 1500

static void ndp_addr_resolution(const char *smac, const char *saddr, 
		const char *daddr, const char *taddr)
{
	int sockfd, opt_len, tot_len;
	struct nd_opt_hdr *opt;
	struct nd_neighbor_advert *na;
	struct nd_neighbor_solicit *ns;
	char buffer[BUFFER_SIZE];

	sockfd = nd_socket(255);   // 跳数限制为 255，保证不会跑远（不能转发或者路由）

	// 构建消息
	opt = nd_alloc_opt_src(smac, &opt_len);               // 设置自己的链接层地址
	ns = nd_alloc_ns(taddr, opt, opt_len); 
	tot_len = sizeof(struct nd_neighbor_solicit) + opt_len;
	// ns->nd_ns_cksum = ipv6_cksum(saddr, daddr, IPPROTO_ICMPV6, ns, tot_len);
	ns->nd_ns_cksum = 0;
	
	// 发送消息
	nd_send(sockfd, ns, tot_len, daddr, 0);
	nd_free_ns(&ns);
	nd_free_opt(&opt);

	// 接收消息
	memset(buffer, 0, sizeof(buffer));
	nd_recv(sockfd, buffer, sizeof(buffer), taddr, 0);

	// 解析消息
	na = (struct nd_neighbor_advert *) buffer;
	opt = (struct nd_opt_hdr *) (buffer + sizeof(struct nd_neighbor_advert));
	nd_print_na(na, opt);

	nd_close(sockfd);
}

int main(int argc, char *argv[])
{
	const char *smac  = "02:3b:7b:b7:3b:2d";          // 发送者链路层地址
	const char *saddr = "fe80::a00:27ff:fe06:5688";   // 本机 IPv6 地址
	const char *daddr = "ff02::1:ff0d:d0d";           // 发给组播地址
	const char *taddr = "fe80::20d:dff:fe0d:d0d";     // 待解析 IPv6 地址
	ndp_addr_resolution(smac, saddr, daddr, taddr);
	return 0;
}
