#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>              //close
#include<string.h>              //memset
#include<errno.h> 
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>           //sockaddr_in

#define MAX_LEN 4096

int main(int argc, char* argv[]){
    
    int serfd, clifd;               //记录服务端的socket fd和接收到的客户端的socket fd
    struct sockaddr_in6 seraddr;     //记录协议地址
    int ret;                        //记录recv的返回值
    char buf[MAX_LEN];              //保存接收到的内容

    if( (serfd = socket(AF_INET6, SOCK_STREAM, 0)) == -1 ){      //TCP
        printf("create socket error: %s(errno: %d)\n",strerror(errno),errno);
        exit(EXIT_FAILURE);
    }

    memset(&seraddr, 0, sizeof(seraddr)); 

    seraddr.sin6_family = AF_INET6;  
    seraddr.sin6_port = htons(6666); 
    if(inet_pton(AF_INET6, "fe80::a00:27ff:fe06:5688", &seraddr.sin6_addr) < 0){
        fprintf(stderr, "inet_pton error\n");
        exit(EXIT_FAILURE);
    }

    //bind地址
    if( bind(serfd, (struct sockaddr*)&seraddr, sizeof(seraddr)) == -1 ){
        printf("bind socket error: %s(errno: %d)\n",strerror(errno),errno); 
        exit(EXIT_FAILURE);
    }

    //开始监听, 最多监听的socket数量是10
    if( listen(serfd, 10) == -1 ){
        printf("listen socket error: %s(errno: %d)\n",strerror(errno),errno); 
        exit(EXIT_FAILURE);
    }

    printf("\033[1;33m|====================Waiting client's Request======================|\033[5m\n");
    while(1){
        if( (clifd = accept(serfd, (struct sockaddr*)NULL, NULL)) == -1 ){
            printf("accept socket error: %s(errno: %d)",strerror(errno),errno); 
            continue;
        }
        ret = recv(clifd, buf, MAX_LEN, 0);     //返回clifd接收内容字节数
        buf[ret] = '\0';                        //结束符
        printf("recv msg from client is: %s\n", buf);

        close(clifd);                           //关闭客服端
    }

    close(serfd);                               //关闭服务端
    return 0;
}