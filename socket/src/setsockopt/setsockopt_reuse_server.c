#include<stdio.h>
#include<stdlib.h>  
#include<string.h>                  /* for bzero */
#include<unistd.h>                  /* for close */
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>              /* for sockaddr_in */


int main(int argc, char* argv[]){
    int sockfd1;
    int bind_ret;
    int acc_fd1, acc_fd2;
    int readlen;    
    char recv_buf1[1000],recv_buf2[1000];            //保存接收到的内容

    sockfd1 = socket(AF_INET,SOCK_DGRAM, 0);
    if(sockfd1 == -1){
        fprintf(stderr, "socket error\n");
        exit(EXIT_FAILURE);
    }

    /* 设置本地网络信息 */
    struct sockaddr_in myaddr;       
    struct sockaddr srcaddr;
    bzero(&myaddr, sizeof(myaddr));

    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(6666);
    myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* 在sockfd1绑定bind之前，设置其端口复用  */
    int opt = 1;
    setsockopt(sockfd1, SOL_SOCKET, SO_REUSEADDR, (const void*)&opt, sizeof(opt));


    /* 绑定，端口为6666 */
    bind_ret = bind(sockfd1, (struct sockaddr*)&myaddr, sizeof(myaddr));
    if(bind_ret == -1){
        fprintf(stderr, "bind error\n");
        exit(EXIT_FAILURE);
    }

    int sockfd2;
    sockfd2 = socket(AF_INET,SOCK_DGRAM, 0);
    if(sockfd2 == -1){
        fprintf(stderr, "socket error\n");
        exit(EXIT_FAILURE);
    }

    /* 在sockfd_two绑定bind之前，设置其端口复用 */
	opt = 1;
	setsockopt( sockfd2, SOL_SOCKET,SO_REUSEADDR, 

					(const void *)&opt, sizeof(opt) );

    // 因为8000端口已被占用，默认情况下，端口没有释放，无法绑定
	bind_ret = bind(sockfd2, (struct sockaddr*)&myaddr, sizeof(myaddr));
	if(bind_ret == -1)
	{
		perror("bind sockfd_two");
		close(sockfd2);		
		exit(EXIT_FAILURE);
	}
    /* UDP 通信不需要listen */

    printf("\033[1;33m|====================Waiting client's Request======================|\033[5m\n");
    while(1){
        
        readlen = recvfrom(sockfd1, recv_buf1, sizeof(recv_buf1), 0, (struct sockaddr*)&myaddr, sizeof(myaddr));
        if(readlen < 0){
            fprintf(stderr, "recv error\n");
            exit(EXIT_FAILURE);
        }
        recv_buf1[readlen] = '\0';                        //结束符
        printf("recvfrom fd1 msg from client is: %s\n", recv_buf1);

        bzero(&recv_buf1, sizeof(recv_buf1));
        int sret = sendto(sockfd1, "Go it!", sizeof("Go it!"), 0, &srcaddr, sizeof(srcaddr));
        if(sret < 0){
            fprintf(stderr, "sendto error\n");
            exit(EXIT_FAILURE);
        }

        // readlen = recv(sockfd2, recv_buf2, sizeof(recv_buf2), 0);
        // if(readlen < 0){
        //     fprintf(stderr, "recv error\n");
        //     exit(EXIT_FAILURE);
        // }
        // recv_buf2[readlen] = '\0';                        //结束符
        // printf("recv fd2  msg from client is: %s\n", recv_buf2);
    }

    close(sockfd1);
    close(sockfd2);

    return 0;
}