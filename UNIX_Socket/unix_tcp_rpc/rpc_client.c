#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include "rpc.h"

int main(int argc, char *argv[])
{
    int client_socket;
    struct sockaddr_un server_addr;

    //1. create socket
    if((client_socket = socket(AF_LOCAL, SOCK_STREAM, 0)) == -1){
        perror("socket");
	exit(EXIT_FAILURE);
    }
    
    //2. config sun_family and sun_path
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sun_family = AF_LOCAL;
    strncpy(server_addr.sun_path, RPC_PATH, sizeof(server_addr.sun_path)-1);
    
    //3. connect
    if(connect(client_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1){
        perror("connect");
	close(client_socket);
	exit(EXIT_FAILURE);
    }

    //4. handle rpc request
    rpc_request_t request;
    rpc_response_t response;

    //ADD
   /* request.op = ADD;
    request.op1 = 10;
    request.op2 = 20;
    send(client_socket, &request, sizeof(request), 0);
    recv(client_socket, &response, sizeof(response), 0);
    printf("ADD Result: %d\n", response.result);
*/
    //SUBTRACT
    request.op = SUBTRACT;
    request.op1 = 99;
    request.op2 = 88;
    send(client_socket, &request, sizeof(request), 0);
    recv(client_socket, &response, sizeof(response), 0);
    printf("SUBTRACT Result: %d\n", response.result);

    close(client_socket);

    return 0;

}
