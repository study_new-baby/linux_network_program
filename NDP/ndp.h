#ifndef __ndp_h_
#define __ndp_h_

#include <stdint.h>
#include <sys/types.h> 

/* 参考 linux /usr/include/netinet/icmp6.h */
#define ND_ROUTER_SOLICIT           133
#define ND_ROUTER_ADVERT            134
#define ND_NEIGHBOR_SOLICIT         135
#define ND_NEIGHBOR_ADVERT          136
#define ND_REDIRECT                 137

#define ND_OPT_SOURCE_LINKADDR      1
#define ND_OPT_TARGET_LINKADDR      2
#define ND_OPT_PREFIX_INFORMATION   3
#define ND_OPT_REDIRECTED_HEADER    4
#define ND_OPT_MTU                  5
#define ND_OPT_RTR_ADV_INTERVAL     7
#define ND_OPT_HOME_AGENT_INFO      8

struct icmp6_hdr {
	uint8_t  icmp6_type;   /* type field */
	uint8_t  icmp6_code;   /* code field */
	uint16_t icmp6_cksum;  /* checksum field */
	union {   
		uint32_t icmp6_un_data32[1]; /* type-specific field */
		uint16_t icmp6_un_data16[2]; /* type-specific field */
		uint8_t  icmp6_un_data8[4];  /* type-specific field */
	} icmp6_dataun;
};  

struct nd_router_solicit      /* router solicitation */
{
	struct icmp6_hdr nd_rs_hdr;
	/* could be followed by options */
};

#define nd_rs_type               nd_rs_hdr.icmp6_type
#define nd_rs_code               nd_rs_hdr.icmp6_code
#define nd_rs_cksum              nd_rs_hdr.icmp6_cksum
#define nd_rs_reserved           nd_rs_hdr.icmp6_data32[0]

struct nd_router_advert       /* router advertisement */
{
	struct   icmp6_hdr nd_ra_hdr;
	uint32_t nd_ra_reachable;   /* reachable time */
	uint32_t nd_ra_retransmit;  /* retransmit timer */
	/* could be followed by options */
};

#define nd_ra_type               nd_ra_hdr.icmp6_type
#define nd_ra_code               nd_ra_hdr.icmp6_code
#define nd_ra_cksum              nd_ra_hdr.icmp6_cksum
#define nd_ra_curhoplimit        nd_ra_hdr.icmp6_data8[0]
#define nd_ra_flags_reserved     nd_ra_hdr.icmp6_data8[1]
#define ND_RA_FLAG_MANAGED       0x80
#define ND_RA_FLAG_OTHER         0x40
#define ND_RA_FLAG_HOME_AGENT    0x20
#define nd_ra_router_lifetime    nd_ra_hdr.icmp6_data16[1]

struct nd_neighbor_solicit    /* neighbor solicitation */
{
	struct icmp6_hdr nd_ns_hdr;
	uint8_t nd_ns_target[16]; /* target address */
	uint8_t nd_ns_options[0];
};

#define nd_ns_type               nd_ns_hdr.icmp6_type
#define nd_ns_code               nd_ns_hdr.icmp6_code
#define nd_ns_cksum              nd_ns_hdr.icmp6_cksum
#define nd_ns_reserved           nd_ns_hdr.icmp6_data32[0]

struct nd_neighbor_advert     /* neighbor advertisement */
{
	struct icmp6_hdr  nd_na_hdr;
	uint8_t nd_na_target[16]; /* target address */
	uint8_t nd_na_options[0]; /* could be followed by options */
};

#define nd_na_type               nd_na_hdr.icmp6_type
#define nd_na_code               nd_na_hdr.icmp6_code
#define nd_na_cksum              nd_na_hdr.icmp6_cksum
#define nd_na_flags_reserved     nd_na_hdr.icmp6_data32[0]
#define ND_NA_FLAG_ROUTER        0x00000080
#define ND_NA_FLAG_SOLICITED     0x00000040
#define ND_NA_FLAG_OVERRIDE      0x00000020

struct nd_redirect            /* redirect */
{
	struct icmp6_hdr  nd_rd_hdr;
	uint8_t nd_rd_target[16]; /* target address */
	uint8_t nd_rd_dst[16];    /* destination address */
	/* could be followed by options */
};

#define nd_rd_type               nd_rd_hdr.icmp6_type
#define nd_rd_code               nd_rd_hdr.icmp6_code
#define nd_rd_cksum              nd_rd_hdr.icmp6_cksum
#define nd_rd_reserved           nd_rd_hdr.icmp6_data32[0]

struct nd_opt_hdr              /* Neighbor discovery option header */
{
	uint8_t  nd_opt_type;
	uint8_t  nd_opt_len;       /* in units of 8 octets */
	uint8_t  nd_opt_data[0];   /* followed by option specific data */
};


struct nd_neighbor_solicit* nd_alloc_ns(const char *taddr, 
		const struct nd_opt_hdr *opt, size_t size);

void nd_free_ns(struct nd_neighbor_solicit **ns);

void nd_print_na(const struct nd_neighbor_advert *na, 
		const struct nd_opt_hdr *tar_opt);


struct nd_opt_hdr *nd_alloc_opt_src(const char *smac, int *size);

void nd_free_opt(struct nd_opt_hdr **opt);


int nd_socket(uint8_t hop_limit);

ssize_t nd_send(int sockfd, const void *data, size_t size, 
		const char *daddr, int flags);

ssize_t nd_recv(int sockfd, void *buf, size_t size, 
		const char *daddr, int flags); 

void nd_close(int sockfd);

#endif /* __ndp_h_ */
