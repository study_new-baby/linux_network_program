#ifndef __SOCKET_UNIX_H__
#define __SOCKET_UNIX_H__

#define UNIX_PATH_MAX    108
struct sockaddr_un { 
    sa_family_t sun_family;               /* AF_UNIX */ 
    char        sun_path[UNIX_PATH_MAX];  /* pathname */ 
};


#endif