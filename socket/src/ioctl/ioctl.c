#include<sys/ioctl.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<net/if.h>     /* for ifconf */

int main(int argc, char *argv[]){
    int sock, if_count, i;
    struct ifconf ifc;
    struct ifreq ifr[10];
    unsigned char mac[6];

    memset(&ifc, 0, sizeof(struct ifconf));

    sock = socket(AF_INET, SOCK_DGRAM, 0);

    ifc.ifc_len = 10 * sizeof(struct ifreq);
    ifc.ifc_buf = (char *)ifr;

    //获取所有网卡信息
    ioctl(sock, SIOCGIFCONF, (char *)&ifc);

    if_count = ifc.ifc_len / (sizeof(struct ifreq));
    for(i = 0; i < if_count; i++){
        if(ioctl(sock, SIOCGIFHWADDR, &ifr[i]) == 0){
            memcpy(mac, ifr[i].ifr_hwaddr.sa_data, 6);
            printf("eth: %s, mac: %02x:%02x:%02x:%02x:%02x:%02x\n", ifr[i].ifr_name, \
            mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
        }
    }
    return 0;
}