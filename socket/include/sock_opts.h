#ifndef __SOCK_OPTS_H__
#define __SOCK_OPTS_H__

#include<netinet/tcp.h>

struct linger {
    int i_onoff;        /* 0=off, nonzero=on */
    int i_linger;       /* linger time as seconds*/
};


union val{
    int i_val;
    long i_val;
    struct linger linger_val;
    struct timeval timeval_val;

}val;

static char *sock_str_flag   (union val *, int);
static char *sock_str_int    (union val *, int);
static char *sock_str_linger (union val *, int);
static char *sock_str_timeval(union val *, int);


struct sock_opts
{
   const char *opt_str;
   int opt_level;
   int opt_name;
   char *(*opt_val_str(union val *, int));
   
}sock_opts[] = {
    {"SO_BROADCAST", SOL_SOCKET, SO_BROADCAST, sock_str_flag},
    {"SO_DEBUG",     SOL_SOCKET, SO_DEBUG,     sock_str_flag},
    {"SO_ERROR",     SOL_SOCKET, SO_DEBUG,     sock_str_int},
    {"SO_RCVBUF",    SOL_SOCKET, SO_RCVBUF,    sock_str_int},
    {"SO_SNDBUF",    SOL_SOCKET, SO_SNDBUF,    sock_str_int},

#ifdef SO_REUSEPORT
    {"SO_REUSEPORT", SOL_SOCKET, SO_REUSEPORT, sock_str_flag},
#else
    {"SO_REUSEPORT",    0,          0,              NULL},
#endif
};


#endif