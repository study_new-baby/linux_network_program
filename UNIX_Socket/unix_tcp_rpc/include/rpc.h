#ifndef __RPC_H_
#define __RPC_H_


#define RPC_PATH	"/home/mantic/Mantic/UNIX_Socket/unix_tcp_rpc/rpc_socket"
#define BUFFER_SIZE 256

typedef enum{
    ADD = 1,
    SUBTRACT = 2
}RPC_CMD;

typedef struct{
   RPC_CMD op;
   int op1;
   int op2;
}rpc_request_t;


typedef struct{
    int result;
}rpc_response_t;

#endif
