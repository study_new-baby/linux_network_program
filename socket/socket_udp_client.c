#include <unistd.h>
#include <pthread.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>

#define DEST_ADDR "192.168.33.112"
#define UDP_PORT 6666
int main()
{
	int ret;
	int read_len = 0;
	int sockfd;
	int server_fd;
	struct sockaddr_in dest_addr;
	struct sockaddr src_addr;
	socklen_t len;
    char buffer[1024];
	
    bzero(&dest_addr, sizeof(dest_addr));
	dest_addr.sin_family = AF_INET; //使用IPV4
	dest_addr.sin_port = htons(UDP_PORT);//服务器端口号
	// dest_addr.sin_addr.s_addr = htonl(INADDR_ANY);//主机可能有多个网卡，INADDR_ANY表示绑定所有网卡,适用于本地通信
	dest_addr.sin_addr.s_addr = inet_addr(DEST_ADDR);//适用于局域网通信
	
	sockfd = socket(AF_INET,SOCK_DGRAM,0);//创建TCP，指定ip类型为IPV4
	if(sockfd < 0)
	{
		printf("socket create fail!\r\n");
		return -1;
	}

    printf("\033[1;33m|=======send msg to server======|\033[5m\n");  
    int ret;                /* for getsockopt */
	while(1)
	{
		fgets(buffer, sizeof(buffer), stdin);
		ret = sendto(sockfd,buffer,strlen(buffer),0,&dest_addr,sizeof(dest_addr));
		if(ret < 0)
		{
            getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &ret, sizeof(ret));    /* check options error state */
		    OSAL_PRINTF("Fatal error:%d\n",ret);
			printf("sendto error\r\n");
			continue;
		}
		memset(buffer,0,sizeof(buffer));
		read_len = recvfrom(sockfd,buffer,sizeof(buffer),0,&src_addr,&len);
		printf("recv:%s\r\n",buffer);
		sleep(1);
	}
	close(sockfd);
	return 0;
}
