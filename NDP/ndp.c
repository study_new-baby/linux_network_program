#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/ether.h>  /* for ether_aton */
#include <net/ethernet.h>

#include "ndp.h"

struct nd_neighbor_solicit* nd_alloc_ns(const char *taddr, 
		const struct nd_opt_hdr *opt, size_t size)
{
	struct sockaddr_in6 addr;
	struct nd_neighbor_solicit *ns;

	ns = (struct nd_neighbor_solicit *) calloc(1, sizeof(struct nd_neighbor_solicit) + size);
	ns->nd_ns_type = ND_NEIGHBOR_SOLICIT;
	ns->nd_ns_code = 0;
	
	if (inet_pton(AF_INET6, taddr, &addr.sin6_addr) == 0) 
		fprintf(stderr, "taddr");
	memcpy(ns->nd_ns_target, &addr.sin6_addr, sizeof(addr.sin6_addr));

	if (NULL != opt && size > 0) 
		memcpy(ns->nd_ns_options, opt, size);
	return ns;
}

void nd_free_ns(struct nd_neighbor_solicit **ns)
{
	if (NULL != ns && NULL != *ns) {
		free(*ns);
		*ns = NULL;
	}
}

void nd_print_na(const struct nd_neighbor_advert *na, 
		const struct nd_opt_hdr *tar_opt) 
{
	char buffer[INET6_ADDRSTRLEN];

	printf("%02x\n", na->nd_na_type);
	printf("%02x\n", na->nd_na_code);
	printf("%04x\n", htons(na->nd_na_cksum));
	
	if (inet_ntop(AF_INET6, na->nd_na_target, buffer, INET6_ADDRSTRLEN) == NULL)
        fprintf(stderr, "inet_ntop");
	printf("%s\n", buffer);
	printf("%s\n", ether_ntoa((struct ether_addr *) tar_opt->nd_opt_data));
}


struct nd_opt_hdr *nd_alloc_opt_src(const char *smac, int *size)
{
	struct ether_addr *addr;
	struct nd_opt_hdr *opt;
	int tot_len = sizeof(struct nd_opt_hdr) + sizeof(struct ether_addr);

	opt = (struct nd_opt_hdr *) calloc(1, tot_len);
	opt->nd_opt_type = ND_OPT_SOURCE_LINKADDR;
	opt->nd_opt_len  = 1;

	addr = ether_aton(smac);
	memcpy(opt->nd_opt_data, addr->ether_addr_octet, sizeof(addr->ether_addr_octet));

	*size = tot_len;
	return opt;
}

void nd_free_opt(struct nd_opt_hdr **opt)
{
	if (NULL != opt && NULL != *opt) {
		free(*opt);
		*opt = NULL;
	}
}


int nd_socket(uint8_t hop_limit) 
{
	int sockfd;
	int hops = hop_limit;

	if ((sockfd = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6)) == -1) 
        fprintf(stderr, "socket");

	if (setsockopt(sockfd, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &hops, sizeof(hops)) == -1)
        fprintf(stderr, "setsockopt : IPV6_HOPLIMIT");
        
	return sockfd;
}

ssize_t nd_send(int sockfd, const void *data, size_t size, 
		const char *daddr, int flags) 
{
	ssize_t count;
	struct sockaddr_in6 addr;

	memset(&addr, 0, sizeof(addr));
	addr.sin6_family = AF_INET6;
	inet_pton(addr.sin6_family, daddr, &addr.sin6_addr);

	if ((count = sendto(sockfd, data, size, flags, (struct sockaddr *)&addr, sizeof(addr))) == -1)
        fprintf(stderr, "sendto");
	return count;
}

ssize_t nd_recv(int sockfd, void *buf, size_t size, 
		const char *daddr, int flags) 
{
	ssize_t count;
	struct sockaddr_in6 addr;
	socklen_t socklen = sizeof(addr);

	memset(&addr, 0, sizeof(addr));
	addr.sin6_family = AF_INET6;
	inet_pton(addr.sin6_family, daddr, &addr.sin6_addr);

	if ((count = recvfrom(sockfd, buf, size, flags, (struct sockaddr *)&addr, &socklen)) == -1)
        fprintf(stderr, "recvfrom");
	return count;
}

void nd_close(int sockfd) 
{
	if (close(sockfd) == -1)
		fprintf(stderr, "taddr");
}
