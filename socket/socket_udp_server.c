#include <unistd.h>
#include <pthread.h>
#include  <netdb.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>

#define UDP_PORT 6666
int main()
{
	int ret, ret2;
	int read_len = 0;
	int thread_fd;
	int sockfd, sockfd2;
	int server_fd;
	struct sockaddr_in server_addr;
	struct sockaddr client_addr;
	socklen_t len;
	char buffer[1024];
    int opt;
	
	server_addr.sin_family = AF_INET; //使用IPV4
	server_addr.sin_port = htons(UDP_PORT);//服务器端口号
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);//主机可能有多个网卡，INADDR_ANY表示绑定所有网卡
	
    /* sock fd 1 */
	sockfd = socket(AF_INET,SOCK_DGRAM,0);//创建TCP，指定ip类型为IPV4
	if(sockfd < 0)
	{
		printf("socket create fail!\r\n");
		return -1;
	}

    /* 在sockfd 绑定bind之前，设置其端口复用 */
	opt = 1;
	setsockopt( sockfd2, SOL_SOCKET,SO_REUSEADDR, 

					(const void *)&opt, sizeof(opt) );

	ret = bind(sockfd,(struct sockaddr*)&server_addr,sizeof(struct sockaddr));//将socket和服务器地址绑定
	if(ret < 0)
	{
		printf("socket bind fail!\r\n");
	}

    /* sock fd 2 */
    sockfd2 = socket(AF_INET,SOCK_DGRAM,0);//创建TCP，指定ip类型为IPV4
	if(sockfd2 < 0)
	{
		printf("socket2 create fail!\r\n");
		return -1;
	}

    /* 在sockfd2绑定bind之前，设置其端口复用 */
	opt = 1;
	setsockopt( sockfd2, SOL_SOCKET,SO_REUSEADDR, 

					(const void *)&opt, sizeof(opt) );


	ret2 = bind(sockfd2,(struct sockaddr*)&server_addr,sizeof(struct sockaddr));//将socket和服务器地址绑定
	if(ret < 0)
	{
		printf("socket2 bind fail!\r\n");
	}

    printf("\033[1;33m|====================Waiting client's Request======================|\033[5m\n");
	while(1)
	{
		memset(buffer,0,sizeof(buffer));
		read_len = recvfrom(sockfd,buffer,sizeof(buffer),0,&client_addr,&len);
		printf("sockfd recv(%d):%s\r\n",read_len,buffer);//最后面如果不带\r\n可能会导致在shell中不显示数据

        memset(buffer,0,sizeof(buffer));
		read_len = recvfrom(sockfd2,buffer,sizeof(buffer),0,&client_addr,&len);
		printf("sockfd2 recv(%d):%s\r\n",read_len,buffer);//最后面如果不带\r\n可能会导致在shell中不显示数据

		if(read_len > 0)
		{
			sendto(sockfd,"Got it!",strlen("Got it!"),0,&client_addr,sizeof(client_addr));
		}
	}
	close(sockfd);
	return 0;
}
