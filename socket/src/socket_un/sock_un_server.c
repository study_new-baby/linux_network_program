#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<sys/un.h>

int main(int argc, char *argv[]){
    int serfd;
    int ret;
    char buf[100] = {0};

    serfd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if(serfd == -1){
        fprintf(stderr, "socket error\n");
        exit(EXIT_FAILURE);
    }

    /* 创建服务端ip地址 */
    struct sockaddr_un server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, "./my_local");  //套节字文件
    socklen_t slen = sizeof(server_addr);


    ret = bind(serfd, (struct sockaddr*)&server_addr, slen);
    if(ret == -1){
        fprintf(stderr, "bind error\n");
        exit(EXIT_FAILURE);
    }

    /* 下面保存客户端的地址 */
    struct sockaddr_un client_addr;
    memset(&client_addr, 0, sizeof(client_addr));
    socklen_t clen = sizeof(client_addr);


    while(1){
        if(-1 == (recvfrom(serfd, buf, sizeof(buf), 0, (struct sockaddr*)&client_addr, &clen))){
            fprintf(stderr, "recvfrom error\n");
            exit(EXIT_FAILURE);
        }
        printf("客户端 (%s) 发来数据:[%s]\n", client_addr.sun_path, buf);

        //组装回复给客户端的应答
        strcat(buf, "---996");
        //发送数据
        if( -1 == (sendto(serfd, buf, sizeof(buf), 0, (struct sockaddr*)&client_addr, clen))){
            fprintf(stderr, "sendto error\n");
            exit(EXIT_FAILURE);
        }

    }

    //关闭监听套接字  一般不关闭
    close(serfd);

    printf("UNIX domain socket bound\n");
    return 0;
}